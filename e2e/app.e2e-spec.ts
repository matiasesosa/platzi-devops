import { PlatziDevopsPage } from './app.po';

describe('platzi-devops App', () => {
  let page: PlatziDevopsPage;

  beforeEach(() => {
    page = new PlatziDevopsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
